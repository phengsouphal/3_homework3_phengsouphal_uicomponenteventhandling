package com.example.gamer.homework3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

//Define Spinner
    Spinner sp;
    // create string array
    String location []={"Phnom Penh","Seam reab","Kompong Cham","Ta Kea"};
    //Define array adapter for string type
    ArrayAdapter <String> arrayAdapter;
    //define String variable for record
    String record="";


    String type=null;


    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;



    @BindView(R.id.saveData)
    Button saveData;


    @BindView(R.id.pickImage)
    Button pickImage;

    @BindView(R.id.imageView)
    ImageView imageView;
    public static final int PICK_IMAGE=100;

    Uri imageUri;

    EditText txttitle,txtweb,txtgmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        int selectedId = radioGroup.getCheckedRadioButtonId();

        txttitle=findViewById(R.id.txttitle);
        txtweb=findViewById(R.id.txtweb);
        txtgmail=findViewById(R.id.txtgmail);


        //PICK Image
        pickImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();


            }
        });


        //Save Data to Second Activity
        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(MainActivity.this,Main2Activity.class);

                String titlevalue=txttitle.getText().toString();
                String webvalue=txtweb.getText().toString();
                String gmailvalue=txtgmail.getText().toString();



                DetailInfo detailInfo=new DetailInfo(titlevalue,type,webvalue,gmailvalue,record,imageUri);

                intent.putExtra("DETAIL",detailInfo);

                startActivityForResult(intent,2);
            }
        });





        sp=(Spinner)findViewById(R.id.spinner);
        arrayAdapter= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,location);
        sp.setAdapter(arrayAdapter);


        //Set spinner method
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //use postion value
                switch (position){
                    case 0:
                        record="Phnom Penh";
                        break;
                    case 1:
                        record="Seam reab";
                        break;
                    case 2:
                        record="Kompong Cham";
                        break;
                    case 3:
                        record="Ta Kea";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch(checkedId){
                    case R.id.raweb:
                        type="វែបសាយ";

                        break;
                    case R.id.rafb:
                        type="ហ្វេសប៊ុក";

                        break;
                }


            }
        });


    }


    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK&&requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imageView.setImageURI(imageUri);




        }


    }
}
