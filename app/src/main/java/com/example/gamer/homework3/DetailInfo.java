package com.example.gamer.homework3;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class DetailInfo implements Parcelable {
    private String title;
    private String webFb;
    private String www;
    private String gmail;
    private String catename;


    private Uri picture;

    public DetailInfo(String title, String webFb, String www, String gmail, String catename, Uri picture) {
        this.title = title;
        this.webFb = webFb;
        this.www = www;
        this.gmail = gmail;
        this.catename = catename;
        this.picture = picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebFb() {
        return webFb;
    }

    public void setWebFb(String webFb) {
        this.webFb = webFb;
    }

    public String getWww() {
        return www;
    }

    public void setWww(String www) {
        this.www = www;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getCatename() {
        return catename;
    }

    public void setCatename(String catename) {
        this.catename = catename;
    }


    public Uri getPicture() {
        return picture;
    }

    public void setPicture(Uri picture) {
        this.picture = picture;
    }

    protected DetailInfo(Parcel in) {
        title = in.readString();
        webFb = in.readString();
        www = in.readString();
        gmail = in.readString();
        catename = in.readString();
        picture = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<DetailInfo> CREATOR = new Creator<DetailInfo>() {
        @Override
        public DetailInfo createFromParcel(Parcel in) {
            return new DetailInfo(in);
        }

        @Override
        public DetailInfo[] newArray(int size) {
            return new DetailInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(webFb);
        dest.writeString(www);
        dest.writeString(gmail);
        dest.writeString(catename);
        dest.writeParcelable(picture, flags);
    }
}