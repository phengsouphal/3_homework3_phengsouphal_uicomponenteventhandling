package com.example.gamer.homework3;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.nio.Buffer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {

    TextView viewTitle,viewWeb,viewGmail,webOrfb,Location;

    @BindView(R.id.showImage)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);


        viewTitle=(TextView)findViewById(R.id.viewtitle);
        viewWeb=(TextView)findViewById(R.id.viewweb);
        viewGmail=(TextView)findViewById(R.id.viewgmail);
        webOrfb=(TextView)findViewById(R.id.weborfb);
        Location=(TextView)findViewById(R.id.location);



        DetailInfo detailInfo=(DetailInfo)getIntent().getParcelableExtra("DETAIL");
        viewTitle.setText("Title:   "+detailInfo.getTitle());
        webOrfb.setText("Type:   "+detailInfo.getWebFb());
        viewWeb.setText("Website:   "+detailInfo.getWww());
        viewGmail.setText("Gmail:   "+detailInfo.getGmail());
        Location.setText(("Location:   "+detailInfo.getCatename()));
        imageView.setImageURI(detailInfo.getPicture());



    }
}
